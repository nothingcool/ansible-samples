E.g:
----
ansible-playbook -i inventory helloworld.yml -e "theHost=uptime.lab.nothing.cool"
ansible-playbook -i inventory  get-ip.yml -e "theHost=uptime.lab.nothing.cool"
ansible-playbook -i inventory delegate-sample.yml -e "theHost=aap-3" -e "theDelegateHost=aap-2" -e "my_user=rmtcm"
ansible-playbook -i inventory delegate-sample.yml -e "theHost=aap-2" -e "theDelegateHost=aap-3" -e "my_user=rmtcm"


ansible-playbook -vvvvvi inventory test-become.yml -e theHost=aap-2 --ask-pass --user tony -e isNextUser=yes -e nextUser="tony1" -e nextUserPass=testing12345 --flush-cache

ansible-playbook -vvvi inventory test-delegate.yml -e theHost=aap-2 --ask-pass --user=root -e delegateUser=rmtcm -e delegateHost=aap-3
